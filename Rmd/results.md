---
title: "Sentiments in Scientific Papers"
output:
  html_document:
    df_print: paged
    keep_md: true
---

There is an accompanying [blog post](https://hohenfeld.is/posts/do-scientific-papers-that-are-positively-written-get-more-citations/) elaborating more on the processing of the data.

For the present analysis scientific papers from the Fontiers family of 
open-aceess journals were scraped, combined with information about the amount
of citations they have receivend and supplemented with information about the 
sentiment in the text.

Run the setup:

```r
source("../R/00_setup.R")
```

```
## here() starts at /Users/christian/random/science_sentiments
```

```
## Loading required package: xml2
```

```
## ── Attaching packages ────────────────────────────────────────────────────────────────────────────────────────────────────── tidyverse 1.2.1 ──
```

```
## ✔ ggplot2 3.2.0     ✔ purrr   0.3.2
## ✔ tibble  2.1.3     ✔ dplyr   0.8.3
## ✔ tidyr   0.8.3     ✔ stringr 1.4.0
## ✔ readr   1.3.1     ✔ forcats 0.4.0
```

```
## ── Conflicts ───────────────────────────────────────────────────────────────────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter()         masks stats::filter()
## ✖ readr::guess_encoding() masks rvest::guess_encoding()
## ✖ dplyr::lag()            masks stats::lag()
## ✖ purrr::pluck()          masks rvest::pluck()
```

Load all necessary data:

```r
citation_numbers <- readRDS(file.path(mydir$save, "citation_numbers.Rds"))
papertbl_sen <- readRDS(file.path(mydir$save, "papertbl_sen.Rds"))
paper_sen <- readRDS(file.path(mydir$save, "paper_sen.Rds"))
```

## Citations
The `citation_numbers` dataset was mainly created for later joining the 
citation number into the `paper_sen` data, but we can get an overview 
about the distribution of citation numbers:


```r
ggplot(citation_numbers) +
    aes(x = citations) +
    geom_density(fill = "black", alpha = .25) +
    scale_x_continuous(breaks = seq(0, 80, 5)) +
    labs(x = "Number of Citations", y = "Density")
```

![](results_files/figure-html/unnamed-chunk-3-1.png)<!-- -->

From the plot we can see that the most common amount of citations in the data 
is between 0 and 5 and rapidly declines after that.

## Sentiment
We can get a quick overview of sentiment by journal and year:

```r
sentiment_summary <- papertbl_sen %>% 
    separate(id, into = c("journal", "year")) %>% 
    group_by(journal, year) %>% 
    summarise(mean_sentiment = mean(value), sd = sd(value))
sentiment_summary
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":["journal"],"name":[1],"type":["chr"],"align":["left"]},{"label":["year"],"name":[2],"type":["chr"],"align":["left"]},{"label":["mean_sentiment"],"name":[3],"type":["dbl"],"align":["right"]},{"label":["sd"],"name":[4],"type":["dbl"],"align":["right"]}],"data":[{"1":"fnins","2":"2016","3":"0.3114268","4":"1.712548"},{"1":"fnins","2":"2017","3":"0.3343448","4":"1.706900"},{"1":"fnins","2":"2018","3":"0.2733913","4":"1.710492"},{"1":"fphys","2":"2016","3":"0.3212608","4":"1.631673"},{"1":"fphys","2":"2017","3":"0.3010335","4":"1.642466"},{"1":"fphys","2":"2018","3":"0.2993357","4":"1.621477"},{"1":"fpsyg","2":"2016","3":"0.3558530","4":"1.780424"},{"1":"fpsyg","2":"2017","3":"0.3140501","4":"1.812702"},{"1":"fpsyg","2":"2018","3":"0.2375575","4":"1.834192"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>

And plot it:

```r
ggplot(sentiment_summary) +
    aes(x = year, y = mean_sentiment, ymin = mean_sentiment - sd,
        ymax = mean_sentiment + sd, colour = journal) +
    geom_hline(yintercept = 0, colour = "grey80") +
    geom_point(position = position_dodge(width = 0.1)) +
    geom_errorbar(position = position_dodge(width = 0.1), width = 0.1) +
    labs(y = "Mean Sentiment (AFINN)", x = "Year", colour = "Journal")
```

![](results_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

From the plot and the table above, we can see that ther does not seem to be much
of a difference between journals and over time with regard to sentiment. 
The mean sentiment is slightly positive, but the standard deviation is rather 
large and crosses well into the negative value range.

As there does not seem to be much of a difference in sentiment over time 
and between journals, let's have another look at paper sections:

```r
section_sentiment <- papertbl_sen %>% 
    group_by(section) %>% 
    summarise(mean_sentiment = mean(value), sd = sd(value))
section_sentiment
```

<div data-pagedtable="false">
  <script data-pagedtable-source type="application/json">
{"columns":[{"label":["section"],"name":[1],"type":["chr"],"align":["left"]},{"label":["mean_sentiment"],"name":[2],"type":["dbl"],"align":["right"]},{"label":["sd"],"name":[3],"type":["dbl"],"align":["right"]}],"data":[{"1":"Discussion","2":"0.3660348","3":"1.740917"},{"1":"Introduction","2":"0.2656921","3":"1.813411"},{"1":"Methods","2":"0.2532617","3":"1.678634"},{"1":"Results","2":"0.3114628","3":"1.625576"}],"options":{"columns":{"min":{},"max":[10]},"rows":{"min":[10],"max":[10]},"pages":{}}}
  </script>
</div>
On average there do not appear to be large differences in sentiment between 
the _standard sections_ of a paper.

## Sentiment and Citations
Now let's have a look at the relation between sentiment data and the amount
of citations papers do get:


```r
ggplot(paper_sen) +
  aes(x = mean, y = citations, colour = year) +
  geom_vline(xintercept = 0, colour = "grey40") +
  geom_point(alpha = .5) +
  facet_grid(section~journal) +
  labs(x = "Mean Sentiment", y = "Number of Citations",
       colour = "Year of\nPublication")
```

![](results_files/figure-html/unnamed-chunk-7-1.png)<!-- -->

From this plot it does not appear like there is much of a relation between 
both variables.

We can furhter investigate with another plot:


```r
ggplot(paper_sen) +
    aes(x = mean, y = citations) +
    geom_point(shape = 21, alpha = .4) +
    geom_smooth() +
    labs(y = "Citations", x = "Sentiment")
```

```
## `geom_smooth()` using method = 'gam' and formula 'y ~ s(x, bs = "cs")'
```

![](results_files/figure-html/unnamed-chunk-8-1.png)<!-- -->

This plot pretty much confirms the above.

The relation can be further characterised using the correlation coefficient:

```r
cor(paper_sen$mean, paper_sen$citations)
```

```
## [1] 0.03093981
```

The correlation is so close to zero that it is safe to say that there is no
relation between sentiment and citations.
