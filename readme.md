# Science Sentiments
This project contains a data analysis of the sentiment in the text of
open-access scientific publications to answer the question whether papers that
are more positively written get more citations.

## Running the analysis
### Dependencies
This analysis requires the following packages and their dependencies:
`here`, `rvest`, `tidytext`, `tidyverse`, `xml2`. 
For running the analysis for the first time you need a working internet 
connection, the .Rds files that are then wirtten to disk need about 50 MB of 
space.

### Running
It should be sufficient to source the numbered files in order, this will take
care of everything you need. 
Depending on your internet connection running the analysis for the first time
might take a while, expect an hour or two. 
If you run the analysis for the second time and data is just read from disk 
everything should be finished in less than a minute on a reasonably recent
machine.

## Results
There is a markdown document outlining the results briefly [in this respository](https://gitlab.com/choh/science-sentiments/blob/e7550b4ee7096f8e65e530506072f888e9ee8681/Rmd/results.md).
There is additionally [a blog post](https://hohenfeld.is/posts/do-scientific-papers-that-are-positively-written-get-more-citations/) describing more of the thought process and data sources used for this project.
